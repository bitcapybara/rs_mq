use crate::{
    AcknowledgeType, FixedStr, InitialPosition, ProducerAccessMode, ServerError, SubscriptionType,
};

#[derive(Debug, serde::Serialize, serde::Deserialize)]
#[serde(tag = "type", content = "cmd")]
pub enum Command {
    // client -> server
    Connect(CommandConnect),
    // server -> client
    Connected(CommandConnected),
    // client -> server
    Subscribe(CommandSubscribe),
    // client -> server
    Producer(CommandProducer),
    // producer -> server
    Send(CommandSend),
    // server -> producer
    SendReceipt(CommandSendReceipt),
    // server -> producer
    SendError(CommandSendError),
    // server -> consumer
    Message(CommandMessage),
    // consumer -> server
    Ack(CommandAck),
    // consumer -> server
    Flow(CommandFlow),
    // consumer -> server
    Unsubscribe(CommandUnsubscribe),
    // server -> client
    Success(CommandSuccess),
    // server -> client
    Error(CommandError),
    // server <-> client
    CloseProducer(CommandCloseProducer),
    // server <-> client
    CloseConsumer(CommandCloseConsumer),
    // server -> client
    ProducerSuccess(CommandProducerSuccess),
    // server <-> client
    Ping(CommandPing),
    // server <-> client
    Pong(CommandPong),
    // client -> server
    GetTopics(CommandGetTopics),
    // server -> client
    GetTopicsResponse(CommandGetTopicsResponse),
}

impl Command {
    pub fn ping() -> Self {
        Command::Ping(CommandPing {})
    }

    pub fn request_id(&self) -> Option<u64> {
        match self {
            Command::Subscribe(CommandSubscribe { request_id, .. }) => Some(request_id),
            Command::Producer(CommandProducer { request_id, .. }) => Some(request_id),
            Command::Send(CommandSend { request_id, .. }) => Some(request_id),
            Command::Ack(CommandAck { request_id, .. }) => Some(request_id),
            Command::Flow(CommandFlow { request_id, .. }) => Some(request_id),
            Command::Unsubscribe(CommandUnsubscribe { request_id, .. }) => Some(request_id),
            Command::CloseProducer(CommandCloseProducer { request_id, .. }) => Some(request_id),
            Command::CloseConsumer(CommandCloseConsumer { request_id, .. }) => Some(request_id),
            Command::GetTopics(CommandGetTopics { request_id, .. }) => Some(request_id),
            _ => None,
        }
        .cloned()
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandConnect {
    pub client_version: FixedStr,
    pub protocol_version: u16,
    pub keep_alive: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandConnected {
    pub server_version: FixedStr,
    pub proto_version: u16,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandSubscribe {
    pub request_id: u64,
    pub consumer_id: u64,
    pub consumer_name: FixedStr,
    pub topic: FixedStr,
    pub sub_name: FixedStr,
    pub sub_type: SubscriptionType,
    pub queue_size: usize,
    pub initial_position: InitialPosition,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandProducer {
    pub request_id: u64,
    pub producer_id: u64,
    pub producer_name: FixedStr,
    pub topic: FixedStr,
    pub access_mode: ProducerAccessMode,
}

// 数据放在 tcp 流的最后，这里只放除了数据外的其他参数
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandSend {
    pub request_id: u64,
    pub producer_id: u64,
    pub topic: FixedStr,
    pub key: Option<FixedStr>,
    pub timestamp: u64,
    pub sequence_id: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandSendReceipt {
    pub producer_id: u64,
    pub sequence_id: u64,
    pub message_id: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandSendError {
    pub producer_id: u64,
    pub sequence_id: u64,
    pub error: ServerError,
    pub message: FixedStr,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandMessage {
    pub consumer_id: u64,
    pub message_id: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandAck {
    pub request_id: u64,
    pub consumer_id: u64,
    pub message_ids: Vec<u64>,
    pub ack_type: AcknowledgeType,
    pub checksum_error: bool,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandFlow {
    pub request_id: u64,
    pub consumer_id: u64,
    pub permits: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandUnsubscribe {
    pub request_id: u64,
    pub consumer_id: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandSuccess {
    pub request_id: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandError {
    pub request_id: u64,
    pub error: ServerError,
    pub message: FixedStr,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandCloseProducer {
    pub request_id: u64,
    pub producer_id: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandCloseConsumer {
    pub request_id: u64,
    pub consumer_id: u64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandProducerSuccess {
    pub request_id: u64,
    pub producer_name: FixedStr,
    pub producer_id: u64,
    pub last_sequence_id: Option<u64>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandPing {}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandPong {}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandGetTopics {
    pub request_id: u64,
    pub pattern: Option<FixedStr>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CommandGetTopicsResponse {
    pub request_id: u64,
    pub topics: Vec<FixedStr>,
}
