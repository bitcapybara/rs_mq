#![allow(dead_code)]
mod command;
mod fixed_str;

pub use command::*;
pub use fixed_str::FixedStr;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum SubscriptionType {
    Exclusive = 1,
    Shared = 2,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum AcknowledgeType {
    Individual = 1,
    Cumulative = 2,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, serde::Serialize, serde::Deserialize)]
pub enum ProducerAccessMode {
    Shared = 1,
    Exclusive = 2,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum InitialPosition {
    Latest = 1,
    Earliest = 2,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum ServerError {
    MissMagicNumber = 1,
    PayloadChecksumError = 2,
    ProducerAlreadyExists = 3,
    ConsumerAlreadyExists = 4,
    ProducerExceeded = 5,
    ProducerConflictAccessMode = 6,
    ConsumerExceeded = 7,
    ConsumerNotFound = 8,
    SendPayloadNotFound = 9,
    AckTypeInvalid = 10,
    ProducerNotFound = 11,
    ConsumerConflictSubType = 12,
    TopicNotFound = 13,
}

impl ServerError {
    pub fn error_code(&self) -> u16 {
        unsafe { *(self as *const ServerError as *const u16) }
    }
}
