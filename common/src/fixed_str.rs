#[derive(Debug, Clone, Eq, PartialEq, Hash, serde::Serialize, serde::Deserialize)]
pub struct FixedStr(Box<str>);

impl FixedStr {
    pub fn as_str(&self) -> &str {
        &self.0
    }
}

impl std::convert::From<String> for FixedStr {
    fn from(s: String) -> Self {
        Self(Box::from(s))
    }
}

impl std::convert::From<&str> for FixedStr {
    fn from(s: &str) -> Self {
        Self(Box::from(s))
    }
}

impl<'a> std::convert::From<&'a FixedStr> for &'a str {
    fn from(s: &'a FixedStr) -> Self {
        s
    }
}

impl std::ops::Deref for FixedStr {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl AsRef<str> for FixedStr {
    fn as_ref(&self) -> &str {
        self
    }
}

impl std::fmt::Display for FixedStr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl PartialEq<String> for FixedStr {
    fn eq(&self, other: &String) -> bool {
        PartialEq::eq(&self.0[..], &other[..])
    }
}

impl<'a> PartialEq<&'a str> for FixedStr {
    fn eq(&self, other: &&'a str) -> bool {
        PartialEq::eq(&self.0[..], &other[..])
    }
}

impl PartialEq<str> for FixedStr {
    fn eq(&self, other: &str) -> bool {
        PartialEq::eq(&self.0[..], other)
    }
}
