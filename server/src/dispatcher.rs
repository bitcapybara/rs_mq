mod consumers;

use consumers::Consumers;
use tokio::sync::RwLock;

use std::sync::Arc;

use gecko_common::SubscriptionType;

use crate::{Consumer, Result};

pub(crate) struct Dispatcher {
    // sub type
    pub(crate) sub_type: SubscriptionType,
    // consumers
    consumers: RwLock<Consumers>,
}

impl Dispatcher {
    pub(crate) fn new(sub_type: SubscriptionType) -> Self {
        Self {
            sub_type,
            consumers: RwLock::new(Consumers::new(sub_type)),
        }
    }

    pub(crate) async fn consumers(&self) -> Vec<Arc<Consumer>> {
        self.consumers.read().await.consumers()
    }

    pub(crate) async fn add_consumer(&self, consumer: Arc<Consumer>) -> Result<()> {
        self.consumers.write().await.add(consumer)
    }

    pub(crate) async fn remove_consumer(&self, consumer: Arc<Consumer>) -> Option<Arc<Consumer>> {
        self.consumers.write().await.remove(&consumer.name)
    }

    pub(crate) async fn consumer_flow(&self, name: &str, permits: u64) -> Result<()> {
        self.consumers.write().await.update_permits(name, permits)
    }

    pub(crate) async fn has_permits(&self) -> bool {
        self.consumers.read().await.has_permits()
    }

    pub(crate) async fn next_available(&self) -> Option<Arc<Consumer>> {
        self.consumers.read().await.next_available()
    }
}
