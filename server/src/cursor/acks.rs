use tokio::sync::RwLock;

use crate::message::MessageId;

use self::{bits::Bits, range::RangeSet};

mod bits;
mod range;

#[derive(Debug, Clone, Copy, serde::Serialize, serde::Deserialize)]
pub(crate) enum AckContainerType {
    Bits,
    RangeSet,
}

// TODO async trait
pub(super) enum Acks {
    Bits(RwLock<Bits>),
    RangeSet(RwLock<RangeSet>),
}

impl Acks {
    pub(super) fn new(ack_container: AckContainerType) -> Self {
        match ack_container {
            AckContainerType::Bits => Self::Bits(RwLock::new(Bits::new())),
            AckContainerType::RangeSet => Self::RangeSet(RwLock::new(RangeSet::new())),
        }
    }

    // 将指定 id 的消息置为 true
    pub(super) async fn set(&self, id: MessageId) {
        match self {
            Acks::Bits(bits) => bits.write().await.set(id),
            Acks::RangeSet(rgs) => rgs.write().await.set(id),
        }
    }

    // 将指定消息及之前多有消息置为 true
    pub(super) async fn set_until(&self, id: MessageId) {
        match self {
            Acks::Bits(bits) => bits.write().await.set_until(id),
            Acks::RangeSet(rgs) => rgs.write().await.set_until(id),
        }
    }

    // 第一个 ack 空洞的位置
    pub(super) async fn delete_position(&self) -> Option<MessageId> {
        match self {
            Acks::Bits(bits) => bits.read().await.first_zero(),
            Acks::RangeSet(rgs) => rgs.read().await.first_zero(),
        }
    }

    // 最后一个 ack 位置 +1
    pub(super) async fn read_position(&self) -> MessageId {
        todo!()
    }

    // 获取多个未 ack 的数据
    pub(super) async fn pop(&self, _limit: usize) -> Vec<MessageId> {
        todo!()
    }
}
