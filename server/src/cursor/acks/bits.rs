use bitvec::{order::Lsb0, prelude::BitVec};

use crate::message::MessageId;

pub(in crate::cursor) struct Bits(BitVec<u8, Lsb0>);

impl Bits {
    pub(super) fn set(&mut self, id: MessageId) {
        let index = id as usize;
        if id < self.first_zero().unwrap_or(0) {
            return;
        }
        let len = self.0.len();
        if index + 1 > len {
            // vec 里，一个 0 是 8 个 0bit
            let vec = vec![0; (index + 1 - len) / 8 + 1];
            self.0.extend_from_raw_slice(&vec);
        }
        self.0.set(index, true);
    }

    pub(super) fn set_until(&mut self, id: MessageId) {
        let index = id as usize;
        if id < self.first_zero().unwrap_or(0) {
            return;
        }
        let len = self.0.len();
        if index + 1 > len {
            // vec 里，一个 0xFF 代表 8个 1bit
            let vec = Self::cal_bit(index + 1 - len);
            println!("vec: {:?}", &vec);
            self.0.extend_from_raw_slice(&vec);
        }
        for i in self.first_zero().unwrap_or(0) as usize..=index.min(len) {
            self.0.set(i, true);
        }
    }

    pub(super) fn first_zero(&self) -> Option<MessageId> {
        self.0.first_zero().map(|i| i as MessageId)
    }
}

impl Bits {
    pub(super) fn new() -> Self {
        Self(BitVec::new())
    }

    // total: 表示生成多少个 1 的 vec
    fn cal_bit(total: usize) -> Vec<u8> {
        let n1 = total / 8;
        let re1 = total - n1 * 8;

        let mut ele = 0u8;
        let mut pow = 1u8;
        for _ in 0..re1 {
            ele += pow;
            pow *= 2;
        }
        let mut res = Vec::with_capacity(n1 + 1);
        res.append(&mut vec![0xff; n1]);
        res.push(ele);
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use bitvec::bits;

    // cargo test cursor::acks::bits::tests::bits_set -- --exact --nocapture
    #[test]
    fn set() {
        let mut bits = Bits::new();

        bits.set(2);
        bits.set(10);
        assert_eq!(Some(0), bits.first_zero());
        assert_eq!(
            bits.0,
            bits![0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
        );

        bits.set(0);
        bits.set(1);
        assert_eq!(Some(3), bits.first_zero());
        assert_eq!(
            bits.0,
            bits![1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
        );
    }

    // cargo test cursor::acks::tests::bits::set_until -- --exact --nocapture
    #[test]
    fn set_until() {
        let mut bits = Bits::new();

        bits.set(13);
        bits.set(15);

        bits.set_until(10);
        assert_eq!(Some(11), bits.first_zero());
        assert_eq!(
            bits.0,
            bits![1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1]
        )
    }

    // cargo test cursor::acks::tests::bits::cal_bit -- --exact --nocapture
    #[test]
    fn cal_bit() {
        let vec = Bits::cal_bit(10);
        assert_eq!(vec, vec![0b11111111, 0b11]);
    }
}
