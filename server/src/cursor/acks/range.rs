use std::{collections::BTreeMap, ops::RangeInclusive};

use crate::message::MessageId;

// {0:[0..5], 6:[6..10]...}
#[derive(Debug)]
pub(in crate::cursor) struct RangeSet(BTreeMap<MessageId, RangeInclusive<MessageId>>);

impl RangeSet {
    pub(super) fn set(&mut self, id: MessageId) {
        match self.floor(id) {
            Some(low) => {
                if id == 0 {
                    return;
                }
                let rg = self.0.get(&low).unwrap();
                let end = rg.end().to_owned();

                // [0..4] <- 4 或 [0..5] <- 4 不做处理
                let mut new_key = low;

                // [0..2] <- 4 => [0..2], [4..4]
                if end < id - 1 {
                    self.0.insert(id, id..=id);
                    new_key = id;
                }

                // [0..3] <- 4  => [0..4]
                if end == id - 1 {
                    self.0.insert(low, low..=id);
                }

                // 处理下一个
                let next_key = id + 1;
                if let Some(next) = self.0.get(&next_key) {
                    let start = next.start().to_owned();
                    let end = next.end().to_owned();
                    // [5..10]
                    if start == id + 1 {
                        self.0.remove(&next_key);
                        self.0.insert(new_key, new_key..=end);
                    }
                }
            }
            None => {
                // 第一次插入，没有找到小于此 id 的 range
                self.0.insert(id, id..=id);
            }
        }
    }

    pub(super) fn set_until(&mut self, id: MessageId) {
        // 保留 [2..5] , [4..10], [6..10]
        let mut end = id;
        // TODO 使用 pop_first 进行替换
        self.0.retain(|k, rg| {
            if k > &id {
                return true;
            }
            let e = rg.end();
            if e > &id {
                end = e.to_owned();
            }
            false
        });
        self.0.insert(0, 0..=end);
    }

    pub(super) fn first_zero(&self) -> Option<MessageId> {
        if self.0.is_empty() {
            return None;
        }
        self.0.get(&0).map(|rg| rg.end() + 1).or(Some(0))
    }
}

impl RangeSet {
    pub(super) fn new() -> Self {
        Self(BTreeMap::new())
    }
    // 获取不大于指定key的第一个key
    fn floor(&self, id: MessageId) -> Option<u64> {
        let mut res = None;
        for &k in self.0.keys() {
            if k > id {
                break;
            }
            res.replace(k);
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // cargo test cursor::acks::range::tests::set -- --exact --nocapture
    #[test]
    fn set() {
        let mut rst = RangeSet::new();
        assert_eq!(None, rst.first_zero());
        rst.set(2);
        rst.set(10);
        assert_eq!(rst.0, BTreeMap::from([(2, 2..=2), (10, 10..=10)]));
        assert_eq!(Some(0), rst.first_zero());

        rst.set(0);
        rst.set(1);
        rst.set(9);
        assert_eq!(rst.0, BTreeMap::from([(0, 0..=2), (9, 9..=10)]));
        assert_eq!(Some(3), rst.first_zero());

        rst.set(3);
        rst.set(11);
        assert_eq!(rst.0, BTreeMap::from([(0, 0..=3), (9, 9..=11)]));
        assert_eq!(Some(4), rst.first_zero());
    }

    // cargo test cursor::acks::range::tests::set_until -- --exact --nocapture
    #[test]
    fn set_until() {
        let mut rst = RangeSet::new();
        assert_eq!(None, rst.first_zero());
        rst.set_until(5);
        assert_eq!(rst.0, BTreeMap::from([(0, 0..=5)]));
        assert_eq!(Some(6), rst.first_zero());
        rst.set_until(10);
        assert_eq!(rst.0, BTreeMap::from([(0, 0..=10)]));
        assert_eq!(Some(11), rst.first_zero());
    }

    // cargo test cursor::acks::range::tests::floor -- --exact --nocapture
    #[test]
    fn floor() {
        let mut rst = RangeSet::new();
        assert_eq!(None, rst.floor(0));
        assert_eq!(None, rst.floor(5));
        rst.set(4);
        assert_eq!(None, rst.floor(0));
        assert_eq!(Some(4), rst.floor(5));
        assert_eq!(Some(4), rst.floor(10));
        rst.set_until(4);
        assert_eq!(Some(0), rst.floor(0));
        assert_eq!(Some(0), rst.floor(5));
    }
}
