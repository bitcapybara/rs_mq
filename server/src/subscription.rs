use std::sync::{atomic, Arc};

use gecko_common::{AcknowledgeType, FixedStr, ServerError, SubscriptionType};
use log::warn;

use crate::{
    cursor::AckContainerType, CommonError, Consumer, Cursor, Dispatcher, Error, MemStore,
    MessageId, Result,
};

// 订阅创建后，不会自动删除，需要手动执行删除命令
pub(crate) struct Subscription {
    // read only
    pub(crate) name: FixedStr,
    // read only
    pub(crate) topic: FixedStr,
    // read only
    pub(crate) sub_type: SubscriptionType,
    // 消息分发策略
    dispatcher: Dispatcher,
    // 表示消费消息进度的游标
    cursor: Cursor,
    // 是否有 consumer_flow 任务在进行中
    has_pending_read: atomic::AtomicBool,
}

impl Subscription {
    pub(crate) fn new(
        topic: &str,
        sub_name: &str,
        sub_type: SubscriptionType,
        ack_container: AckContainerType,
        msg_store: Arc<MemStore>,
    ) -> Self {
        Self {
            name: sub_name.into(),
            topic: topic.into(),
            sub_type,
            dispatcher: Dispatcher::new(sub_type),
            cursor: Cursor::new(sub_type, ack_container, msg_store),
            has_pending_read: atomic::AtomicBool::new(false),
        }
    }

    pub(crate) async fn add_consumer(&self, consumer: Arc<Consumer>) -> Result<()> {
        self.dispatcher.add_consumer(consumer).await
    }

    pub(crate) async fn consumer_num(&self) -> usize {
        self.dispatcher.consumers().await.len()
    }

    // 直到没有消息可以发送后再退出
    pub(crate) async fn consumer_flow(&self, consumer_name: &str, permits: u64) -> Result<()> {
        // update consumer permits
        self.dispatcher
            .consumer_flow(consumer_name, permits)
            .await?;

        self.push_message().await;

        Ok(())
    }

    // 向消费者推送消息
    // 消费者主动拉取(flow)或生产者发送消息(send)后触发
    // 使用 atomic 保证此方法串行执行
    pub(crate) async fn push_message(&self) {
        // 如果有任务在进行，直接退出
        if self.has_pending_read.load(atomic::Ordering::SeqCst) {
            return;
        }
        self.has_pending_read.store(true, atomic::Ordering::SeqCst);

        // 开始发送消息给消费者，不管是哪种订阅模式，每个消息只发给一个消费者
        // TODO 性能优化，多个消息并行发送
        while self.dispatcher.has_permits().await {
            let c = self.dispatcher.next_available().await.unwrap();
            // 消费者的 permits
            let permits = c.permits();
            // 待发送的消息
            let messages = self.cursor.get_messages(permits as usize).await;
            for msg in messages.into_iter() {
                let msg_id = msg.id;
                match c.send_message(msg).await {
                    Ok(_) => {
                        // 投递成功，设置cursor.acks 和 consumer.pending_acks
                    }
                    Err(e) => {
                        warn!(
                            "push message {} to consumer {} error: {}",
                            msg_id, &c.name, e
                        );
                    }
                }
            }
        }

        self.has_pending_read.store(false, atomic::Ordering::SeqCst)
    }

    fn is_individual_ack_mode(&self) -> bool {
        self.sub_type == SubscriptionType::Shared
    }

    pub(crate) async fn ack(&self, ack_type: AcknowledgeType, msg_ids: &[MessageId]) -> Result<()> {
        if ack_type == AcknowledgeType::Cumulative && self.is_individual_ack_mode() {
            return Err(Error::Server(CommonError::new(
                ServerError::AckTypeInvalid,
                "cumulative ack to shared sub mode",
            )));
        }
        match ack_type {
            AcknowledgeType::Individual => {
                self.cursor.ack(msg_ids).await;
            }
            AcknowledgeType::Cumulative => {
                let id = msg_ids
                    .get(0)
                    .ok_or_else(|| Error::system("msg id is empty"))?;
                self.cursor.ack_until(id).await;
            }
        }
        Ok(())
    }

    pub(crate) async fn mark_delete_position(&self) -> Option<MessageId> {
        self.cursor.delete_position().await
    }

    // 删除 dispather 的 consumer
    // 将 consumer 中的 pending_acks 重置 cursor.unack
    pub(crate) async fn remove_consumer(&self, consumer: Arc<Consumer>) -> Result<()> {
        let _consumer = self.dispatcher.remove_consumer(consumer).await;

        Ok(())
    }
}
