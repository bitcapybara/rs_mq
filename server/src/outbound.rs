use tokio::sync::mpsc;

use gecko_common::{
    Command, CommandConnected, CommandError, CommandGetTopicsResponse, CommandMessage, CommandPing,
    CommandPong, CommandProducerSuccess, CommandSuccess, FixedStr,
};

use crate::{
    codec, CommandPayload, CommonError, Payload, RawMessage, Result, PROTOCOL_VERSION,
    SERVER_VERSION,
};

mod sender;

#[derive(Clone)]
pub(crate) struct Outbound(sender::CmdSender);

impl Outbound {
    pub(crate) fn new(tx: mpsc::Sender<CommandPayload>) -> Self {
        Self(sender::CmdSender::new(Some(tx)))
    }

    pub(crate) fn noop() -> Self {
        Self(sender::CmdSender::new(None))
    }

    pub(crate) async fn send_ping(&self) -> Result<()> {
        self.send(Command::Ping(CommandPing {}), None).await
    }

    pub(crate) async fn send_pong(&self) -> Result<()> {
        self.send(Command::Pong(CommandPong {}), None).await
    }

    pub(crate) async fn send_connected(&self) -> Result<()> {
        self.send(
            Command::Connected(CommandConnected {
                server_version: SERVER_VERSION.into(),
                proto_version: PROTOCOL_VERSION,
            }),
            None,
        )
        .await
    }

    pub(crate) async fn send_producer_seccess(
        &self,
        request_id: u64,
        producer_name: &str,
        producer_id: u64,
        last_sequence_id: Option<u64>,
    ) -> Result<()> {
        self.send(
            Command::ProducerSuccess(CommandProducerSuccess {
                request_id,
                producer_name: producer_name.into(),
                producer_id,
                last_sequence_id,
            }),
            None,
        )
        .await
    }

    pub(crate) async fn send_error(&self, request_id: u64, err: CommonError) -> Result<()> {
        self.send(
            Command::Error(CommandError {
                request_id,
                error: err.code,
                message: err.message,
            }),
            None,
        )
        .await
    }

    pub(crate) async fn send_success(&self, request_id: u64) -> Result<()> {
        self.send(Command::Success(CommandSuccess { request_id }), None)
            .await
    }

    pub(crate) async fn send_get_topics_response(
        &self,
        request_id: u64,
        topics: Vec<FixedStr>,
    ) -> Result<()> {
        self.send(
            Command::GetTopicsResponse(CommandGetTopicsResponse { request_id, topics }),
            None,
        )
        .await
    }

    pub(crate) async fn send_message(&self, consumer_id: u64, msg: RawMessage) -> Result<()> {
        self.send(
            Command::Message(CommandMessage {
                consumer_id,
                message_id: msg.id,
            }),
            Some(codec::encode_message(msg)?),
        )
        .await
    }

    async fn send(&self, cmd: Command, pd: Option<Vec<u8>>) -> Result<()> {
        let cp = CommandPayload::new(cmd, pd.map(Payload::new));
        self.0.send(cp).await
    }
}
