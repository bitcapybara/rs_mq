use gecko_common::{FixedStr, ProducerAccessMode};

use crate::AckContainerType;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub(crate) struct Config {
    pub(crate) port: u16,
    pub(crate) topics: Vec<Topic>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub(crate) struct Topic {
    pub(crate) name: FixedStr,
    pub(crate) access_mode: ProducerAccessMode,
    pub(crate) ack_holder: AckContainerType,
}

impl Config {
    pub(crate) fn from_file(_path: &str) -> Self {
        todo!()
    }
}
