use anyhow::Context;
use gecko_common::Command;

use crate::{RawMessage, Result};

pub(crate) fn decode_cmd(data: &[u8]) -> Result<Command> {
    Ok(rmp_serde::from_slice::<Command>(data).context("decode cmd error")?)
}

pub(crate) fn encode_cmd(cmd: Command) -> Result<Vec<u8>> {
    Ok(rmp_serde::to_vec(&cmd).context("encode cmd error")?)
}

pub(crate) fn encode_message(msg: RawMessage) -> Result<Vec<u8>> {
    Ok(rmp_serde::to_vec(&msg).context("encode rawmessage error")?)
}

pub(crate) fn crc32(data: &[u8]) -> u32 {
    crc32fast::hash(data)
}
