mod producers;
mod subscriptions;

use producers::Producers;
use subscriptions::Subscriptions;
use tokio::sync::RwLock;

use std::sync::Arc;

use gecko_common::{AcknowledgeType, FixedStr, ProducerAccessMode, ServerError};

use crate::{
    AckContainerType, CommonError, Consumer, Error, MemStore, MessageId, Producer, RawMessage,
    Result,
};

const MAX_CONSUMER_PER_TOPIC: usize = 1000;

pub(crate) struct Topic {
    pub(crate) name: FixedStr,
    pub(crate) mode: ProducerAccessMode,

    // subscriptions
    subscriptions: Subscriptions,

    // producers
    producers: RwLock<Producers>,

    // messages
    msg_store: Arc<MemStore>,
}

impl Topic {
    pub(crate) fn new(
        name: &str,
        access_mode: ProducerAccessMode,
        ack_container: AckContainerType,
        msg_store: Arc<MemStore>,
    ) -> Self {
        Topic {
            name: name.into(),
            mode: access_mode,
            subscriptions: Subscriptions::new(ack_container),
            producers: RwLock::new(Producers::new(access_mode)),
            msg_store,
        }
    }

    pub(crate) async fn add_producer(&self, producer: Arc<Producer>) -> Result<()> {
        self.producers.write().await.add(producer)
    }

    pub(crate) async fn subscribe(&self, consumer: Arc<Consumer>) -> Result<()> {
        let sub_name = consumer.subscription.clone();
        let sub_type = consumer.sub_type;
        let subscription = self
            .subscriptions
            .get_or_create(&sub_name, sub_type, &self.name, self.msg_store.clone())
            .await;

        if self.consumer_exceeded().await {
            return Err(Error::Server(CommonError::new(
                ServerError::ProducerExceeded,
                "Topic consumers exceeded",
            )));
        }
        subscription.add_consumer(consumer.clone()).await
    }

    async fn consumer_exceeded(&self) -> bool {
        let mut consumer_nums = 0;
        for sub in self.subscriptions.subs().await {
            consumer_nums += sub.consumer_num().await;
        }

        consumer_nums >= MAX_CONSUMER_PER_TOPIC
    }

    pub(crate) async fn consumer_flow(
        &self,
        sub_name: &str,
        consumer_name: &str,
        permits: u64,
    ) -> Result<()> {
        let sub = self.subscriptions.get(sub_name).await?;
        sub.consumer_flow(consumer_name, permits).await
    }

    pub(crate) async fn producer_send(&self, msg: RawMessage) -> Result<()> {
        self.msg_store.add(msg).await;
        for sub in self.subscriptions.subs().await {
            let sub = sub.clone();
            tokio::spawn(async move { sub.push_message().await });
        }
        Ok(())
    }

    pub(crate) async fn consumer_ack(
        &self,
        consumer: Arc<Consumer>,
        ack_type: AcknowledgeType,
        msg_ids: &[MessageId],
    ) -> Result<()> {
        let sub_name = &consumer.subscription;
        let subscription = self.subscriptions.get(sub_name).await?;

        subscription.ack(ack_type, msg_ids).await?;

        // 找到所有subscription消费的最低点，删除此点之前的数据
        let mut mark_delete = None;
        for sub in self.subscriptions.subs().await {
            if let Some(id) = sub.mark_delete_position().await {
                if id < mark_delete.map_or(MessageId::MAX, |m| m) {
                    mark_delete = Some(id);
                }
            }
        }
        if let Some(id) = mark_delete {
            self.msg_store.delete_to(id).await;
        }

        Ok(())
    }

    pub(crate) async fn remove_consumer(&self, consumer: Arc<Consumer>) -> Result<()> {
        let sub_name = &consumer.subscription;
        let subscription = self.subscriptions.get(sub_name).await?;
        subscription.remove_consumer(consumer.clone()).await?;

        if subscription.consumer_num().await == 0 {
            self.subscriptions.remove(&consumer.name).await;
        }

        Ok(())
    }

    pub(crate) async fn remove_producer(&self, producer: Arc<Producer>) -> Result<()> {
        self.producers.write().await.remove(&producer.name)
    }
}
