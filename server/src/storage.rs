use std::collections::BTreeMap;

use tokio::sync::RwLock;

use crate::{MessageId, RawMessage};

// 消息存储（内存实现）
pub(crate) struct MemStore {
    messages: RwLock<BTreeMap<MessageId, RawMessage>>,
}

impl MemStore {
    pub(crate) fn new() -> Self {
        Self {
            messages: RwLock::new(BTreeMap::new()),
        }
    }

    pub(crate) async fn add(&self, msg: RawMessage) {
        self.messages.write().await.insert(msg.id, msg);
    }

    pub(crate) async fn get(&self, from: MessageId, limit: usize) -> Vec<RawMessage> {
        let msgs = self.messages.read().await;
        msgs.range(from..from + limit as MessageId)
            .map(|(_, msg)| msg.to_owned())
            .collect()
    }

    pub(crate) async fn delete_to(&self, msg_id: MessageId) {
        let mut msgs = self.messages.write().await;
        msgs.retain(|&a, _| a < msg_id);
    }

    pub(crate) async fn get_msgs(&self, msg_ids: &[MessageId]) -> Vec<RawMessage> {
        let msgs = self.messages.read().await;
        let mut res = Vec::with_capacity(msg_ids.len());
        for id in msg_ids {
            if let Some(msg) = msgs.get(id) {
                res.push(msg.to_owned());
            }
        }
        res
    }
}
