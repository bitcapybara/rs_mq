use std::{
    collections::HashMap,
    sync::{atomic, Arc},
};

use anyhow::Context;
use gecko_common::{AcknowledgeType, FixedStr};
use tokio::sync::RwLock;

use crate::{
    error::CommonError, Consumer, Error, MemStore, MessageId, Producer, RawMessage, Result, Topic,
};

pub(crate) struct Broker {
    // 当前服务内的所有 topic，处理客户端Topic相关请求
    topics: RwLock<HashMap<FixedStr, Arc<Topic>>>,
    // message store
    msg_store: Arc<MemStore>,
    // message id
    message_id: atomic::AtomicU64,
}

impl Broker {
    pub(crate) fn new() -> Self {
        // TODO 初始化所有 Topic
        Self {
            topics: RwLock::new(HashMap::new()),
            msg_store: Arc::new(MemStore::new()),
            message_id: atomic::AtomicU64::new(0),
        }
    }

    pub(crate) async fn add_producer(&self, producer: Arc<Producer>) -> Result<()> {
        let topic = self.get_topic(&producer.topic).await?;
        topic.add_producer(producer.clone()).await
    }

    pub(crate) async fn subscribe(&self, consumer: Arc<Consumer>) -> Result<()> {
        let topic = self.get_topic(&consumer.topic).await?;
        topic.subscribe(consumer).await
    }

    pub(crate) async fn get_topics(&self, pattern: Option<FixedStr>) -> Result<Vec<FixedStr>> {
        let topicslock = self.topics.read().await;
        let topicsmap = topicslock.keys().map(|name| name.to_owned());

        Ok(match pattern {
            Some(p) => {
                let re = regex::Regex::new(&p).context("topic pattern invalid")?;
                topicsmap.filter(|name| re.is_match(name)).collect::<_>()
            }
            None => topicsmap.collect::<_>(),
        })
    }

    pub(crate) async fn consumer_flow(&self, consumer: Arc<Consumer>, permits: u64) -> Result<()> {
        let topic = &consumer.topic;
        let topic = self.get_topic(topic).await?;
        topic
            .consumer_flow(&consumer.subscription, &consumer.name, permits)
            .await
    }

    pub(crate) async fn producer_send(&self, topic_name: &str, message: RawMessage) -> Result<()> {
        let topic = self.get_topic(topic_name).await?;
        topic.producer_send(message).await
    }

    pub(crate) fn next_message_id(&self) -> u64 {
        self.message_id.fetch_add(1, atomic::Ordering::SeqCst)
    }

    pub(crate) async fn consumer_ack(
        &self,
        consumer: Arc<Consumer>,
        ack_type: AcknowledgeType,
        msg_ids: &[MessageId],
    ) -> Result<()> {
        let topic_name = consumer.topic.clone();
        let topic = self.get_topic(&topic_name).await?;
        topic.consumer_ack(consumer, ack_type, msg_ids).await
    }

    pub(crate) async fn remove_consumer(&self, consumer: Arc<Consumer>) -> Result<()> {
        let topic_name = &consumer.topic;
        let topic = self.get_topic(topic_name).await?;
        topic.remove_consumer(consumer).await
    }

    pub(crate) async fn remove_producer(&self, producer: Arc<Producer>) -> Result<()> {
        let topic = self.get_topic(&producer.topic).await?;
        topic.remove_producer(producer).await
    }

    async fn get_topic(&self, name: &str) -> Result<Arc<Topic>> {
        self.topics
            .read()
            .await
            .get(&name.into())
            .map(|t| t.to_owned())
            .ok_or_else(|| Error::Server(CommonError::miss_topic(name)))
    }
}
