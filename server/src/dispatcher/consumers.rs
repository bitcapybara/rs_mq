use std::{collections::HashMap, sync::Arc};

use gecko_common::{FixedStr, SubscriptionType};

use crate::{CommonError, Consumer, Error, Result};
const MAX_CONSUMER_PER_SUBSCRIPTON: usize = 100;

pub(crate) enum Consumers {
    Single(Option<Arc<Consumer>>),
    Multiple(HashMap<FixedStr, Arc<Consumer>>),
}

impl Consumers {
    pub(crate) fn new(sub_type: SubscriptionType) -> Self {
        match sub_type {
            SubscriptionType::Exclusive => Consumers::Single(None),
            SubscriptionType::Shared => Consumers::Multiple(HashMap::new()),
        }
    }
    pub(crate) fn mode(&self) -> SubscriptionType {
        match self {
            Consumers::Single(_) => SubscriptionType::Exclusive,
            Consumers::Multiple(_) => SubscriptionType::Shared,
        }
    }

    pub(crate) fn update_permits(&self, name: &str, permits: u64) -> Result<()> {
        match self {
            Consumers::Single(Some(c)) => {
                c.set_permits(permits);
                Ok(())
            }
            Consumers::Multiple(cs) => match cs.get(&name.into()) {
                Some(c) => {
                    c.set_permits(permits);
                    Ok(())
                }
                None => Err(Error::Server(CommonError::miss_consumer(name))),
            },
            Consumers::Single(None) => Err(Error::Server(CommonError::miss_consumer(name))),
        }
    }

    pub(crate) fn has_permits(&self) -> bool {
        match self {
            Consumers::Single(Some(c)) => c.permits() > 0,
            Consumers::Multiple(vc) => vc.values().any(|c| c.permits() > 0),
            Consumers::Single(None) => false,
        }
    }

    pub(crate) fn consumers(&self) -> Vec<Arc<Consumer>> {
        match self {
            Consumers::Single(Some(c)) => vec![c.to_owned()],
            Consumers::Multiple(mcs) => {
                let mut res = Vec::with_capacity(mcs.len());
                for c in mcs.values() {
                    res.push(c.clone());
                }
                res
            }
            Consumers::Single(None) => vec![],
        }
    }

    pub(crate) fn add(&mut self, consumer: Arc<Consumer>) -> Result<()> {
        match self {
            Consumers::Multiple(cs) => {
                if SubscriptionType::Shared != consumer.sub_type {
                    return Err(Error::Server(CommonError::consumer_conflict_mode()));
                }
                let name = &consumer.name;
                if cs.contains_key(name) {
                    return Err(Error::Server(CommonError::consumer_duplicate(name)));
                }
                cs.insert(consumer.name.clone(), consumer);
                Ok(())
            }
            Consumers::Single(opc) => match opc {
                Some(c) => Err(Error::Server(CommonError::consumer_duplicate(&c.name))),
                None => {
                    opc.replace(consumer);
                    Ok(())
                }
            },
        }
    }

    pub(crate) fn remove(&mut self, consumer_name: &str) -> Option<Arc<Consumer>> {
        match self {
            Consumers::Single(opc) => {
                if let Some(c) = opc {
                    if c.name == consumer_name {
                        return opc.take();
                    }
                }
                None
            }
            Consumers::Multiple(cs) => cs.remove(&consumer_name.into()),
        }
    }

    pub(crate) fn next_available(&self) -> Option<Arc<Consumer>> {
        match self {
            Consumers::Single(Some(c)) => {
                if c.permits() == 0 {
                    return None;
                }
                Some(c.to_owned())
            }
            Consumers::Multiple(cs) => {
                for c in cs.values() {
                    if c.permits() > 0 {
                        return Some(c.to_owned());
                    }
                }
                None
            }
            Consumers::Single(None) => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::outbound::Outbound;

    use gecko_common::InitialPosition;

    // cargo test dispatcher::consumers::tests::consumers_mode -- --exact --nocapture
    #[test]
    fn consumers_mode() {
        let consumers1 = Consumers::new(SubscriptionType::Exclusive);
        assert_eq!(consumers1.mode(), SubscriptionType::Exclusive);

        let consumers2 = Consumers::new(SubscriptionType::Shared);
        assert_eq!(consumers2.mode(), SubscriptionType::Shared);
    }

    fn new_consumer(id: u64, name: &str, sub_type: SubscriptionType) -> Consumer {
        Consumer::new(
            id,
            name,
            "topic_test",
            "sub_test",
            sub_type,
            InitialPosition::Latest,
            Outbound::noop(),
        )
    }
}
