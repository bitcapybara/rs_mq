use gecko_common::FixedStr;

pub(crate) type MessageId = u64;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub(crate) struct RawMessage {
    // 消息 id
    pub id: MessageId,
    // topic
    pub topic: FixedStr,
    // 消息的生产者
    pub producer: FixedStr,
    // 生产者的消息序列号
    pub sequence_id: u64,
    // 消息生产时间
    pub timestamp: u64,
    // 消息 key
    pub key: Option<FixedStr>,
    // 消息的内容
    pub data: Vec<u8>,
}
