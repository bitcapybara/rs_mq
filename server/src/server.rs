use std::sync::Arc;

use anyhow::Context;
use gecko_common::FixedStr;
use log::*;
use tokio::{
    net::TcpListener,
    sync::{broadcast, mpsc},
};

use crate::{Broker, CommandPayload, Outbound, Protocol, Result, Session};

// tcp 服务
pub(crate) struct Server {
    addr: FixedStr,
    broker: Arc<Broker>,
}

impl Server {
    pub(crate) fn new(addr: &str) -> Self {
        Self {
            addr: addr.into(),
            broker: Arc::new(Broker::new()),
        }
    }

    pub(crate) async fn start(&self) -> Result<()> {
        let listener = TcpListener::bind(self.addr.as_str())
            .await
            .with_context(|| format!("server bind addr {} error", self.addr))?;

        loop {
            let (conn, addr) = listener.accept().await.context("server accept error")?;
            info!("server accept connection from {}", addr);
            let broker = self.broker.clone();
            tokio::spawn(async move {
                let (inbound_tx, inbound_rx) = mpsc::channel::<CommandPayload>(1);
                let (outbound_tx, outbound_rx) = mpsc::channel::<CommandPayload>(1);
                let (close_tx, close_rx) = broadcast::channel::<()>(1);

                let session = Session::new(Outbound::new(outbound_tx), broker);
                let session_c = close_tx.clone();
                let session_jh = tokio::spawn(async move {
                    session.run(inbound_rx, close_rx).await;
                    session_c.send(())
                });

                let protocol_jh = tokio::spawn(async move {
                    Protocol
                        .run(conn, inbound_tx, outbound_rx, close_tx.subscribe())
                        .await;
                    close_tx.send(())
                });

                tokio::join!(session_jh, protocol_jh)
            });
        }
    }
}
