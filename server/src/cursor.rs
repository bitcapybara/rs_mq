use std::sync::Arc;

use gecko_common::SubscriptionType;

use crate::{MemStore, MessageId, RawMessage};

pub(crate) use acks::AckContainerType;
use acks::Acks;

mod acks;

// 每个订阅持有一个游标
// subscription 已存在，consumer 重新连接后，从 unack 的 cursor 开始消费
// 新创建的 subscription，使用 init_position 初始化
pub(crate) struct Cursor {
    sub_type: SubscriptionType,
    // 记录所有记录的ack状态
    // 1 代表已 ack 或 pending_ack 的
    // 0 代表投递失败的或还未读到的
    acks: Acks,
    // 消息存储 从外部传入，全局共用一个
    msg_store: Arc<MemStore>,
}

impl Cursor {
    // TODO 根据 init_position 进行初始化
    pub(crate) fn new(
        sub_type: SubscriptionType,
        ack_container: AckContainerType,
        msg_store: Arc<MemStore>,
    ) -> Self {
        Self {
            sub_type,
            acks: Acks::new(ack_container),
            msg_store,
        }
    }

    // subtype == exclusive，获取 read_position 之后 未 ack 的消息
    // subtype == shared, 获取 delete_position 之后 未 ack 的消息
    pub(crate) async fn get_messages(&self, _limit: usize) -> Vec<RawMessage> {
        todo!()
    }

    // 单条消息 ack
    pub(crate) async fn ack(&self, _msg_ids: &[MessageId]) {
        todo!()
    }

    // 消息累积 ack
    pub(crate) async fn ack_until(&self, _msg_id: &MessageId) {
        todo!()
    }

    // 把指定的消息重置为 unack 状态，等待重新投递
    pub(crate) async fn unack(&self, _consumer_name: &str) {
        todo!()
    }

    // 投递成功的消息，置为 pending 状态，在当前消费者断开连接前，不会再投递给其他消费者
    pub(crate) async fn set_pending(&self, _msg_id: MessageId) {
        todo!()
    }

    // 获取当前 cursor 保存的 ack 最低点位置
    pub(crate) async fn delete_position(&self) -> Option<MessageId> {
        self.acks.delete_position().await
    }
}
