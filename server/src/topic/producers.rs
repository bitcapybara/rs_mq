use std::{collections::HashMap, sync::Arc};

use gecko_common::{FixedStr, ProducerAccessMode};

use crate::{CommonError, Error, Producer, Result};

const MAX_PRODUCER_PER_TOPIC: usize = 100;

pub(crate) enum Producers {
    Single(Option<Arc<Producer>>),
    Multiple(HashMap<FixedStr, Arc<Producer>>),
}

impl Producers {
    pub(crate) fn new(access_mode: ProducerAccessMode) -> Self {
        match access_mode {
            ProducerAccessMode::Exclusive => Producers::Single(None),
            ProducerAccessMode::Shared => Producers::Multiple(HashMap::new()),
        }
    }

    pub(crate) fn mode(&self) -> ProducerAccessMode {
        match self {
            Producers::Multiple(_) => ProducerAccessMode::Shared,
            Producers::Single(_) => ProducerAccessMode::Exclusive,
        }
    }

    pub(crate) fn add(&mut self, producer: Arc<Producer>) -> Result<()> {
        match self {
            Producers::Single(opp) => match opp {
                Some(_) => Err(Error::Server(CommonError::producer_duplicate(
                    &producer.name,
                ))),
                None => {
                    if producer.access_mode != ProducerAccessMode::Exclusive {
                        return Err(Error::Server(CommonError::producer_conflict_mode()));
                    }
                    opp.replace(producer);
                    Ok(())
                }
            },
            Producers::Multiple(ps) => {
                let name = &producer.name;
                if producer.access_mode != ProducerAccessMode::Shared {
                    return Err(Error::Server(CommonError::producer_conflict_mode()));
                }
                if ps.contains_key(name) {
                    return Err(Error::Server(CommonError::producer_duplicate(name)));
                }
                ps.insert(producer.name.clone(), producer);
                Ok(())
            }
        }
    }

    pub(crate) fn remove(&mut self, name: &str) -> Result<()> {
        match self {
            Producers::Single(opp) => {
                if let Some(p) = opp {
                    if p.name == name {
                        opp.take();
                    }
                }
            }
            Producers::Multiple(ps) => {
                ps.remove(&name.into());
            }
        }
        Ok(())
    }
}
