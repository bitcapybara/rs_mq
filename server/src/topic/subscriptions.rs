use std::{collections::HashMap, sync::Arc};

use gecko_common::{FixedStr, SubscriptionType};
use tokio::sync::RwLock;

use crate::{cursor::AckContainerType, storage::MemStore, Error, Result, Subscription};

pub(super) struct Subscriptions {
    ack_container: AckContainerType,
    subs: RwLock<HashMap<FixedStr, Arc<Subscription>>>,
}

impl Subscriptions {
    pub(super) fn new(ack_container: AckContainerType) -> Self {
        Self {
            ack_container,
            subs: RwLock::new(HashMap::new()),
        }
    }

    pub(super) async fn get(&self, name: &str) -> Result<Arc<Subscription>> {
        self.subs
            .read()
            .await
            .get(&name.into())
            .map(|s| s.to_owned())
            .ok_or_else(|| {
                Error::system(format!(
                    "consumer associated subscription {} not found",
                    name
                ))
            })
    }

    pub(super) async fn add(&self, sub: Arc<Subscription>) {
        self.subs.write().await.insert(sub.name.clone(), sub);
    }

    pub(super) async fn get_or_create(
        &self,
        sub_name: &str,
        sub_type: SubscriptionType,
        topic: &str,
        msg_store: Arc<MemStore>,
    ) -> Arc<Subscription> {
        let mut subs = self.subs.write().await;
        if let Some(s) = subs.get(&sub_name.into()) {
            return s.clone();
        }
        let sub = Arc::new(Subscription::new(
            topic,
            sub_name,
            sub_type,
            self.ack_container,
            msg_store,
        ));
        subs.insert(sub_name.into(), sub.clone());
        sub
    }

    pub(super) async fn subs(&self) -> Vec<Arc<Subscription>> {
        self.subs
            .read()
            .await
            .values()
            .map(|s| s.to_owned())
            .collect::<Vec<_>>()
    }

    pub(super) async fn remove(&self, name: &str) {
        self.subs.write().await.remove(&name.into());
    }
}
