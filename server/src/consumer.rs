use std::sync::atomic;

use gecko_common::{FixedStr, InitialPosition, SubscriptionType};
use tokio::sync::RwLock;

use crate::{message::MessageId, outbound::Outbound, RawMessage, Result};

pub(crate) struct Consumer {
    pub(crate) id: u64,
    pub(crate) name: FixedStr,
    pub(crate) topic: FixedStr,
    pub(crate) subscription: FixedStr,
    pub(crate) sub_type: SubscriptionType,
    pub(crate) init_position: InitialPosition,
    permits: atomic::AtomicU64,
    pending_acks: RwLock<Vec<MessageId>>,
    outbound: Outbound,
}

impl Consumer {
    pub(crate) fn new(
        id: u64,
        name: &str,
        topic: &str,
        sub_name: &str,
        sub_type: SubscriptionType,
        init_position: InitialPosition,
        outbound: Outbound,
    ) -> Self {
        Self {
            id,
            name: name.into(),
            topic: topic.into(),
            subscription: sub_name.into(),
            sub_type,
            permits: atomic::AtomicU64::new(0),
            init_position,
            pending_acks: RwLock::new(Vec::new()),
            outbound,
        }
    }

    pub(crate) fn permits(&self) -> u64 {
        self.permits.load(atomic::Ordering::SeqCst)
    }

    pub(crate) fn dec_permit(&self) {
        self.permits.fetch_sub(1, atomic::Ordering::SeqCst);
    }

    pub(crate) fn set_permits(&self, permits: u64) {
        self.permits.store(permits, atomic::Ordering::SeqCst);
    }

    pub(crate) async fn send_message(&self, msg: RawMessage) -> Result<()> {
        // 发送给消费者
        self.outbound.send_message(self.id, msg).await
    }

    pub(crate) async fn add_pending(&self, _id: MessageId) {
        todo!()
    }

    pub(crate) async fn add_pendings(&self, _ids: &[MessageId]) {
        todo!()
    }

    pub(crate) async fn pending_msgs(&self) -> Vec<MessageId> {
        // pop all
        todo!()
    }
}
