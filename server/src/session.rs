mod types;

use types::{Entities, Keepalive};

use std::{sync::Arc, time};

use gecko_common::*;
use log::*;
use tokio::{
    sync::mpsc,
    sync::{broadcast, RwLock},
};

use crate::{
    Broker, CommandPayload, CommonError, Consumer, Error, Outbound, Payload, Producer, RawMessage,
    Result,
};

pub(crate) struct Session {
    // 消息出口
    outbound: Outbound,
    // 管理所有的 Topic
    broker: Arc<Broker>,
    // 会话超时
    keepalive: RwLock<Keepalive>,
    // consumers
    consumers: Entities<Consumer>,
    // producers
    producers: Entities<Producer>,
}

impl Session {
    pub(crate) fn new(outbound: Outbound, broker: Arc<Broker>) -> Self {
        Self {
            outbound,
            broker,
            keepalive: RwLock::new(Keepalive::new()),
            consumers: Entities::new(),
            producers: Entities::new(),
        }
    }
    pub(crate) async fn run(
        self,
        mut inbound_rx: mpsc::Receiver<CommandPayload>,
        mut close: broadcast::Receiver<()>,
    ) {
        let mut timer = tokio::time::interval(time::Duration::from_secs(5));
        let session = Arc::new(self);
        loop {
            tokio::select! {
                cp = inbound_rx.recv() => {
                    if let Some(cp) = cp {
                        let s = session.clone();
                        // 单开线程来处理
                        tokio::spawn(async move {
                            if let Err(e) = s.handle(cp).await {
                                error!("handle command error: {}", e)
                            }
                        });
                    }
                }
                _ = timer.tick() => {
                    if session.keepalive.read().await.is_expire() {
                        error!("client keepalive expired");
                        break
                    }
                    if let Err(e) = session.clone().outbound.send_ping().await {
                        error!("session send ping error: {}", e)
                    }
                }
                _ = close.recv() =>{
                    break
                }
            }
        }
        warn!("session loop exit")
    }

    async fn handle(&self, cp: CommandPayload) -> Result<()> {
        let request_id = cp.cmd.request_id();
        let res = match cp.cmd {
            Command::Ping(_) => self.handle_ping().await,
            Command::Pong(_) => self.handle_pong().await,
            Command::Connect(_) => self.handle_connect().await,
            Command::Producer(cmd) => self.handle_producer(cmd).await,
            Command::Subscribe(cmd) => self.handle_subscribe(cmd).await,
            Command::GetTopics(cmd) => self.handle_get_topic(cmd).await,
            Command::Flow(cmd) => self.handle_flow(cmd).await,
            Command::Send(cmd) => return self.handle_send(cmd, cp.payload).await,
            Command::Ack(cmd) => self.handle_ack(cmd).await,
            Command::Unsubscribe(cmd) => self.handle_unsubscribe(cmd).await,
            Command::CloseProducer(cmd) => self.handle_close_producer(cmd).await,
            Command::CloseConsumer(cmd) => self.handle_close_consumer(cmd).await,
            _ => Err(Error::system("unsupported cmd")),
        };

        if let Err(Error::Server(e)) = res {
            if let Some(req_id) = request_id {
                return self.outbound.send_error(req_id, e).await;
            }
        }
        Ok(())
    }

    // 客户端发来的 ping
    async fn handle_ping(&self) -> Result<()> {
        self.outbound.send_pong().await
    }

    // 客户端发来的 pong
    async fn handle_pong(&self) -> Result<()> {
        self.keepalive.write().await.update();
        Ok(())
    }

    async fn handle_connect(&self) -> Result<()> {
        info!("new client connected");
        self.outbound.send_connected().await
    }

    async fn handle_producer(&self, cmd: CommandProducer) -> Result<()> {
        let producer_id = cmd.producer_id;
        let producer_name = cmd.producer_name;
        let access_mode = cmd.access_mode;
        let request_id = cmd.request_id;

        let topic_name = cmd.topic;

        let producer = Arc::new(Producer::new(
            producer_id,
            &topic_name,
            &producer_name,
            access_mode,
        ));

        self.broker.add_producer(producer.clone()).await?;

        self.outbound
            .send_producer_seccess(
                request_id,
                &producer_name,
                producer_id,
                Some(producer.last_sequence_id()),
            )
            .await
    }

    async fn handle_subscribe(&self, cmd: CommandSubscribe) -> Result<()> {
        let consumer_id = cmd.consumer_id;
        let consumer_name = cmd.consumer_name;
        let request_id = cmd.request_id;
        let topic_name = cmd.topic;

        let sub_name = cmd.sub_name;
        let sub_type = cmd.sub_type;

        let consumer = Arc::new(Consumer::new(
            consumer_id,
            &consumer_name,
            &topic_name,
            &sub_name,
            sub_type,
            cmd.initial_position,
            self.outbound.clone(),
        ));

        self.broker.subscribe(consumer.clone()).await?;

        self.consumers.add(consumer_id, consumer).await;

        // 返回成功结果
        self.outbound.send_success(request_id).await
    }

    async fn handle_get_topic(&self, cmd: CommandGetTopics) -> Result<()> {
        let pattern = cmd.pattern;

        let topics = self.broker.get_topics(pattern).await?;

        self
            .outbound
            .send_get_topics_response(cmd.request_id, topics)
            .await
    }

    async fn handle_flow(&self, cmd: CommandFlow) -> Result<()> {
        let consumer_id = cmd.consumer_id;
        let permits = cmd.permits;
        match self.consumers.get(consumer_id).await {
            Some(c) => self.broker.consumer_flow(c, permits).await,
            None => Err(Error::Server(CommonError::miss_consumer(consumer_id))),
        }
    }

    async fn handle_send(&self, cmd: CommandSend, payload: Option<Payload>) -> Result<()> {
        if payload.is_none() {
            return Err(Error::Server(CommonError::miss_payload()));
        }
        let payload = payload.unwrap();
        let data = payload.data()?;
        let topic_name = cmd.topic;

        let producer_id = cmd.producer_id;
        match self.producers.get(producer_id).await {
            Some(p) => {
                let message = RawMessage {
                    id: self.broker.next_message_id(),
                    topic: topic_name.clone(),
                    producer: p.name(),
                    sequence_id: cmd.sequence_id,
                    timestamp: cmd.sequence_id,
                    key: cmd.key,
                    data,
                };
                self.broker.producer_send(&topic_name, message).await
            }
            None => Err(Error::Server(CommonError::miss_producer(producer_id))),
        }
    }

    async fn handle_ack(&self, cmd: CommandAck) -> Result<()> {
        let consumer = self
            .consumers
            .get(cmd.consumer_id)
            .await
            .ok_or_else(|| Error::system(format!("consumer {} not found", &cmd.consumer_id)))?;

        self.broker
            .consumer_ack(consumer, cmd.ack_type, &cmd.message_ids)
            .await
    }

    async fn handle_unsubscribe(&self, cmd: CommandUnsubscribe) -> Result<()> {
        let consumer_id = cmd.consumer_id;
        let request_id = cmd.request_id;

        match self.consumers.get(consumer_id).await {
            Some(c) => {
                // 处理 subscription
                self.broker.remove_consumer(c).await?;
                self.outbound.send_success(request_id).await
            }
            None => Err(Error::Server(CommonError::miss_consumer(consumer_id))),
        }
    }

    async fn handle_close_producer(&self, cmd: CommandCloseProducer) -> Result<()> {
        let producer_id = cmd.producer_id;
        let request_id = cmd.request_id;

        if let Some(p) = self.producers.get(producer_id).await {
            self.broker.remove_producer(p).await?;
            self.producers.remove(producer_id).await;
        }

        self.outbound.send_success(request_id).await
    }

    async fn handle_close_consumer(&self, cmd: CommandCloseConsumer) -> Result<()> {
        let consumer_id = cmd.consumer_id;
        let request_id = cmd.request_id;

        if let Some(c) = self.consumers.get(consumer_id).await {
            // 处理 subscription
            self.broker.remove_consumer(c).await?;
            // 删除 session 的 consumer
            self.consumers.remove(consumer_id).await;
        }

        self.outbound.send_success(request_id).await
    }
}
