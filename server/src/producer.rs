use std::sync::atomic;

use gecko_common::{FixedStr, ProducerAccessMode};

// 可以给客户端 producer 发送消息
pub(crate) struct Producer {
    // read only
    pub(crate) id: u64,
    // read only
    pub(crate) topic: FixedStr,
    // read only
    pub(crate) name: FixedStr,
    // read only
    pub(crate) access_mode: ProducerAccessMode,
    // 当前生产者的消息序列号
    sequence_id: atomic::AtomicU64,
}

impl Producer {
    pub(crate) fn new(id: u64, topic: &str, name: &str, access_mode: ProducerAccessMode) -> Self {
        Self {
            id,
            topic: topic.into(),
            name: name.into(),
            access_mode,
            sequence_id: atomic::AtomicU64::new(0),
        }
    }
    pub(crate) fn last_sequence_id(&self) -> u64 {
        self.sequence_id.load(atomic::Ordering::SeqCst)
    }

    pub(crate) fn name(&self) -> FixedStr {
        self.name.clone()
    }
}
