use anyhow::{anyhow, Context};
use gecko_common::Command;

use crate::{codec, CommonError, Error, Result, MAGIC_NUMBER};

// magic_bytes(2 bytes) | checksum(4bytes) | payload
pub(crate) struct Payload(Vec<u8>);

impl Payload {
    pub(crate) fn new(data: Vec<u8>) -> Self {
        Self(data)
    }

    pub(crate) fn data(&self) -> Result<Vec<u8>> {
        let magic = self.read_magic()?;
        if magic != MAGIC_NUMBER {
            return Err(Error::Server(CommonError::miss_magic(magic)));
        }
        let checksum = self.read_checksum()?;
        let payload = self
            .0
            .get(8..)
            .ok_or_else(|| anyhow!("failed to get payload, index: {:?}", 8..))?;
        let cs = codec::crc32(payload);
        if cs != checksum {
            return Err(Error::Server(CommonError::checksum_error()));
        }
        Ok(payload.to_vec())
    }

    fn read_magic(&self) -> Result<u16> {
        let slice = self
            .0
            .get(0..2)
            .ok_or(anyhow!("failed to get checksum from slice"))?;
        let array = <[u8; 2]>::try_from(slice).context("failed to convert slice to magic array")?;
        Ok(u16::from_be_bytes(array))
    }

    fn read_checksum(&self) -> Result<u32> {
        let slice = self
            .0
            .get(2..6)
            .ok_or(anyhow!("failed to get checksum from slice"))?;
        let array =
            <[u8; 4]>::try_from(slice).context("failed to convert slice to checksum array")?;
        Ok(u32::from_be_bytes(array))
    }
}

pub(crate) struct CommandPayload {
    pub(crate) cmd: Command,
    pub(crate) payload: Option<Payload>,
}

impl CommandPayload {
    pub(crate) fn new(cmd: Command, payload: Option<Payload>) -> Self {
        Self { cmd, payload }
    }
}
