#![allow(dead_code)]
mod broker;
mod codec;
mod config;
mod consumer;
mod cursor;
mod dispatcher;
mod error;
mod message;
mod outbound;
mod producer;
mod protocol;
mod server;
mod session;
mod storage;
mod subscription;
mod topic;

pub(crate) use broker::Broker;
pub(crate) use consumer::Consumer;
pub(crate) use cursor::{AckContainerType, Cursor};
pub(crate) use dispatcher::Dispatcher;
pub(crate) use error::{CommonError, Error, Result};
pub(crate) use message::{MessageId, RawMessage};
pub(crate) use outbound::Outbound;
pub(crate) use producer::Producer;
pub(crate) use protocol::{CommandPayload, Payload, Protocol};
pub(crate) use session::Session;
pub(crate) use storage::MemStore;
pub(crate) use subscription::Subscription;
pub(crate) use topic::Topic;

const PROTOCOL_VERSION: u16 = 1;
const SERVER_VERSION: &str = "mq_server_broker_0.1.0";
const MAGIC_NUMBER: u16 = 0x37CD;
