use std::fmt::{Debug, Display};

use gecko_common::{FixedStr, ServerError};

pub(crate) type Result<T> = anyhow::Result<T, Error>;

#[derive(Debug, thiserror::Error)]
pub(crate) enum Error {
    // 服务端的业务错误，需要返回给客户端
    #[error("{0}")]
    Server(CommonError),

    // 系统错误，来自标准库或第三方库
    #[error("system error: {0}")]
    System(#[from] anyhow::Error),
}

impl Error {
    pub(crate) fn system<T: Into<String>>(msg: T) -> Self {
        Self::System(anyhow::anyhow!(msg.into()))
    }
}

#[derive(Debug)]
pub(crate) struct CommonError {
    pub(crate) code: ServerError,
    pub(crate) message: FixedStr,
}

impl CommonError {
    pub(crate) fn new(code: ServerError, msg: &str) -> Self {
        Self {
            code,
            message: msg.into(),
        }
    }
    pub(crate) fn checksum_error() -> Self {
        Self {
            code: ServerError::PayloadChecksumError,
            message: "payload checksum error".into(),
        }
    }

    pub(crate) fn miss_magic(magic: u16) -> Self {
        Self {
            code: ServerError::MissMagicNumber,
            message: format!("miss magic number: {:?}", magic).into(),
        }
    }

    pub(crate) fn miss_consumer(consumer: impl Display) -> Self {
        Self {
            code: ServerError::ConsumerNotFound,
            message: format!("consumer with id {} not connected yet", consumer).into(),
        }
    }

    pub(crate) fn miss_producer(producer_id: u64) -> Self {
        Self {
            code: ServerError::ProducerNotFound,
            message: format!("producer with id {} not connected yet", producer_id).into(),
        }
    }

    pub(crate) fn miss_payload() -> Self {
        Self {
            code: ServerError::SendPayloadNotFound,
            message: "receive send command without payload".into(),
        }
    }

    pub(crate) fn producer_conflict_mode() -> Self {
        Self {
            code: ServerError::ProducerConflictAccessMode,
            message: "producer has conflict mode with topic".into(),
        }
    }

    pub(crate) fn consumer_conflict_mode() -> Self {
        Self {
            code: ServerError::ConsumerConflictSubType,
            message: "consumer has conflict sub type with subscription".into(),
        }
    }

    pub(crate) fn producer_duplicate(name: &str) -> Self {
        Self {
            code: ServerError::ProducerAlreadyExists,
            message: format!("producer with name {} already exists", name).into(),
        }
    }

    pub(crate) fn consumer_duplicate(name: &str) -> Self {
        Self {
            code: ServerError::ConsumerAlreadyExists,
            message: format!("consumer with name {} already exists", name).into(),
        }
    }

    pub(crate) fn miss_topic(name: &str) -> Self {
        Self {
            code: ServerError::TopicNotFound,
            message: format!("topic {} not created yet", name).into(),
        }
    }
}

impl Display for CommonError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "ServerError {{ code: {}, message: {} }}",
            self.code.error_code(),
            self.message
        )
    }
}
