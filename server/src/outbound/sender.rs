use tokio::sync::mpsc;

use crate::{CommandPayload, Error, Result};

#[derive(Clone)]
pub(super) enum CmdSender {
    Noop,
    Channel(mpsc::Sender<CommandPayload>),
}

impl CmdSender {
    pub(super) fn new(channel: Option<mpsc::Sender<CommandPayload>>) -> Self {
        match channel {
            Some(ch) => Self::Channel(ch),
            None => Self::Noop,
        }
    }

    pub(super) async fn send(&self, cp: CommandPayload) -> Result<()> {
        match self {
            CmdSender::Noop => Ok(()),
            CmdSender::Channel(sender) => sender
                .send(cp)
                .await
                .map_err(|e| Error::system(format!("outbound rx dropped: {}", e))),
        }
    }
}
