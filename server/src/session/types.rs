use std::{collections::HashMap, ops::Add, sync::Arc, time};

use tokio::sync::RwLock;

// 持有客户端的一次会话（客户端连接）
// TODO dummy protocol
// 业务逻辑处理都放在broker层

// 超过 30s 没有收到心跳回复，则断开客户端
const KEEPALIVE_INTERVAL: time::Duration = time::Duration::from_millis(30000);

pub(super) struct Keepalive {
    expire_at: time::Instant,
}

impl Keepalive {
    pub(super) fn new() -> Self {
        Self {
            expire_at: time::Instant::now().add(KEEPALIVE_INTERVAL),
        }
    }

    pub(super) fn update(&mut self) {
        self.expire_at = self.expire_at.add(KEEPALIVE_INTERVAL);
    }

    pub(super) fn is_expire(&self) -> bool {
        self.expire_at < time::Instant::now()
    }
}

pub(super) struct Entities<T>(RwLock<HashMap<u64, Arc<T>>>);

impl<T> Entities<T> {
    pub(super) fn new() -> Self {
        Self(RwLock::new(HashMap::new()))
    }

    pub(super) async fn get(&self, id: u64) -> Option<Arc<T>> {
        self.0.read().await.get(&id).map(|e| e.to_owned())
    }

    pub(super) async fn add(&self, id: u64, entity: Arc<T>) {
        self.0.write().await.insert(id, entity);
    }

    pub(super) async fn remove(&self, id: u64) {
        self.0.write().await.remove(&id);
    }

    pub(super) async fn has(&self, id: u64) -> bool {
        self.0.read().await.contains_key(&id)
    }
}
