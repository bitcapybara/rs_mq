use anyhow::Context;
use gecko_common::Command;
use log::error;
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
    sync::{broadcast, mpsc},
};

use crate::{codec, Result, MAGIC_NUMBER};

pub(crate) use self::command::{CommandPayload, Payload};

mod command;

/// tcp 流协议(big endian)
///
/// 无 payload:
/// total_size(4 bytes) | cmd_size(4 bytes) | cmd
///
/// 有 payload:
/// total_size(4 bytes) | cmd_size(4 bytes) | cmd | magic_bytes(2 bytes) | checksum(4bytes) | payload
pub(crate) struct Protocol;

impl Protocol {
    // 不断从 conn 读取数据，并通过 tx 发送出去
    // 不断从 send_xx 读取数据，写入 conn
    pub(crate) async fn run(
        self,
        mut conn: TcpStream,
        inbound_tx: mpsc::Sender<CommandPayload>,
        mut outbound_rx: mpsc::Receiver<CommandPayload>,
        mut close: broadcast::Receiver<()>,
    ) {
        loop {
            tokio::select! {
                _ = conn.readable() => {
                    match Self::parse_command(&mut conn).await {
                        Ok(cp) => {
                            if let Err(e) = inbound_tx.send(cp).await {
                                error!("inbound receiver dropped: {}", e);
                                break
                            }
                        },
                        Err(e) => {
                            error!("read tcpstream error: {}", e)
                        }
                    }
                }
                _ = conn.writable() => {
                    if outbound_rx.recv().await.is_none() {
                        error!("outbound channel closed");
                        break
                    }
                }
                _ = close.recv() => {
                    break
                }
            }
        }

        // TODO 断开连接时，需要通知 cursor 重新分配断开连接的 consumer 关联的 pending ack 消息
    }

    async fn parse_command(conn: &mut TcpStream) -> Result<CommandPayload> {
        // 总大小
        let total_size = conn
            .read_u32()
            .await
            .context("conn read total size error")?;
        // 命令大小
        let cmd_size = conn.read_u32().await.context("conn read cmd size error")?;
        // 解析命令
        let mut buf = vec![0u8; cmd_size as usize];
        conn.read(&mut buf).await.context("conn read cmd error")?;
        let cmd = codec::decode_cmd(&buf)?;
        if total_size == cmd_size + 4 {
            return Ok(CommandPayload { cmd, payload: None });
        }
        // 剩下的都读到 vec
        let mut payload_buf = vec![0u8; total_size as usize - cmd_size as usize - 8];
        conn.read(&mut payload_buf)
            .await
            .context("conn read payload error")?;
        Ok(CommandPayload::new(cmd, Some(Payload::new(payload_buf))))
    }

    async fn send_payload_command(
        mut conn: TcpStream,
        cmd: Command,
        payload: Vec<u8>,
    ) -> Result<()> {
        let cmd_res = codec::encode_cmd(cmd)?;
        let res_cmd_size = cmd_res.len() as u32;
        let magic = MAGIC_NUMBER;
        let checksum = codec::crc32(&payload);

        // total_size(4 bytes) | cmd_size(4 bytes) | cmd | magic_bytes(2 bytes) | checksum(4bytes) | payload
        let res_total_size = 4 + 4 + res_cmd_size + 2 + 4 + payload.len() as u32;

        conn.write_u32(res_total_size)
            .await
            .context("conn write total size error")?;
        conn.write_u32(res_cmd_size)
            .await
            .context("conn write cmd size error")?;
        conn.write(&cmd_res).await.context("conn write cmd error")?;
        conn.write_u16(magic)
            .await
            .context("conn write magic num error")?;
        conn.write_u32(checksum)
            .await
            .context("conn write checksum error")?;
        conn.write(&payload)
            .await
            .context("conn write payload error")?;

        conn.flush().await.context("conn flush error")?;

        Ok(())
    }

    async fn send_simple_command(mut conn: TcpStream, cmd: Command) -> Result<()> {
        let cmd_res = codec::encode_cmd(cmd)?;
        let res_cmd_size = cmd_res.len() as u32;
        let res_total_size = res_cmd_size + 4;

        conn.write_u32(res_total_size)
            .await
            .context("conn write total size error")?;
        conn.write_u32(res_cmd_size)
            .await
            .context("conn write cmd size error")?;
        conn.write(&cmd_res).await.context("conn write cmd error")?;

        conn.flush().await.context("conn flush error")?;

        Ok(())
    }
}
