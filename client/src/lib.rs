#![allow(dead_code)]
mod client;
mod consumer;
mod error;
mod producer;

pub use client::Client;
pub use consumer::{Consumer, ConsumerBuilder, TopicSpec};
pub use error::Result;
pub use gecko_common::{FixedStr, SubscriptionType};
pub use producer::{Producer, ProducerBuilder};
