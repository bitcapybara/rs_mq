use log::*;
use std::time;

use crate::{ConsumerBuilder, FixedStr, ProducerBuilder, SubscriptionType, TopicSpec};

const DEFAULT_CONNECT_TIMEOUT: time::Duration = time::Duration::from_secs(5);
const DEFAULT_REQUEST_TIMEOUT: time::Duration = time::Duration::from_secs(10);

pub struct ClientBuilder {
    broker_addr: FixedStr,
    connect_timeout: time::Duration,
    request_timeout: time::Duration,
}

impl ClientBuilder {
    pub fn new(broker_addr: &str) -> Self {
        Self {
            broker_addr: broker_addr.into(),
            connect_timeout: DEFAULT_CONNECT_TIMEOUT,
            request_timeout: DEFAULT_REQUEST_TIMEOUT,
        }
    }

    pub fn connect_timeout(mut self, connect_timeout: time::Duration) -> Self {
        self.connect_timeout = connect_timeout;
        self
    }

    pub fn request_timeout(mut self, request_timeout: time::Duration) -> Self {
        self.request_timeout = request_timeout;
        self
    }

    pub fn build(self) -> Client {
        Client {
            broker_addr: self.broker_addr,
            connect_timeout: self.connect_timeout,
            request_timeout: self.request_timeout,
        }
    }
}

/// 代表一个客户端
/// 1. 包含 broker 连接地址等配置
/// 2. 生成 producer 和 consumer
pub struct Client {
    broker_addr: FixedStr,
    connect_timeout: time::Duration,
    request_timeout: time::Duration,
}

impl Client {
    // 生成一个 producer
    pub fn new_producer(&self, topic: &str, name: &str) -> ProducerBuilder {
        ProducerBuilder::new(topic, name)
    }

    // 生成一个 consumer
    pub fn new_consumer(
        &self,
        topic: TopicSpec,
        sub_name: &str,
        sub_type: SubscriptionType,
    ) -> ConsumerBuilder {
        ConsumerBuilder::new(topic, sub_name, sub_type)
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        debug!("client drop");
    }
}
