use crate::{FixedStr, Result};

pub struct ProducerBuilder {
    topic: FixedStr,
    name: FixedStr,
}

impl ProducerBuilder {
    pub fn new(topic: &str, name: &str) -> Self {
        Self {
            topic: topic.into(),
            name: name.into(),
        }
    }

    // 创建一个 producer，建立长连接
    pub fn create<T>(self) -> Result<Producer<T>> {
        Ok(Producer { data: vec![] })
    }
}

struct MessageToPublish<T> {
    topic: FixedStr,
    key: Option<FixedStr>,
    timestamp: u64,
    sequence_id: u64,
    data: T,
}

/// 代表一个生产者
/// T 是消息类型
pub struct Producer<T> {
    data: Vec<T>,
}

impl<T> Producer<T> {
    pub fn is_connected(&self) -> bool {
        true
    }

    pub async fn send(&self, _: T) -> Result<()> {
        Ok(())
    }

    pub async fn send_with_key(&self, _: T, _: &str) -> Result<()> {
        Ok(())
    }

    pub async fn flush(&mut self) -> Result<()> {
        Ok(())
    }

    pub async fn close(&mut self) -> Result<()> {
        Ok(())
    }
}
