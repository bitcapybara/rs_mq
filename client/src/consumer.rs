use log::*;
use std::time;

use crate::{FixedStr, Result, SubscriptionType};

#[derive(Debug)]
pub enum TopicSpec {
    // 指定 topic
    Topic(FixedStr),
    // 指定 topic 的正则表达式
    Pattern(FixedStr),
    // 指定 topic 列表
    Topics(Vec<FixedStr>),
}

pub struct ConsumerBuilder {
    topic: TopicSpec,
    sub_name: FixedStr,
    sub_type: SubscriptionType,
    queue_size: usize,
}

impl ConsumerBuilder {
    pub fn new(topic: TopicSpec, sub_name: &str, sub_type: SubscriptionType) -> Self {
        Self {
            topic,
            sub_name: sub_name.into(),
            sub_type,
            queue_size: 1000,
        }
    }

    // 创建一个 consumer，建立长连接
    pub fn subscribe<T>(self) -> Result<Consumer<T>> {
        Ok(Consumer { data: vec![] })
    }
}

pub type MessageId = u64;

// 消费者获取的消息
pub struct MessageReceived<T> {
    // 消息 id
    pub id: MessageId,
    // topic
    pub topic: FixedStr,
    // 消息的生产者
    pub producer: FixedStr,
    // 消息生产时间
    pub timestamp: u64,
    // 消息 key
    pub key: Option<FixedStr>,
    // 消息的内容
    pub data: T,
}

/// 代表一个消费者
/// T 是消息类型
pub struct Consumer<T> {
    data: Vec<T>,
}

impl<T> Consumer<T> {
    pub fn is_connected(&self) -> bool {
        true
    }

    pub async fn acknowledge(&self, _: MessageId) -> Result<()> {
        Ok(())
    }

    pub async fn acknowledge_cumulative(&self, _: MessageId) -> Result<()> {
        Ok(())
    }

    pub async fn receive(&self) -> Result<MessageReceived<T>> {
        todo!()
    }

    pub async fn receive_timeout(&self, _: time::Duration) -> Result<MessageReceived<T>> {
        todo!()
    }

    pub async fn unsubscribe(&self) -> Result<()> {
        todo!()
    }

    pub async fn close(&self) -> Result<()> {
        Ok(())
    }
}

impl<T> Drop for Consumer<T> {
    fn drop(&mut self) {
        debug!("consumer dropped");
    }
}
